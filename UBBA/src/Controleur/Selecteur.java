package Controleur;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import Modele.Champion;
import Modele.Jeu;
import Vue.VueArene;
import Vue.VueManager;

public class Selecteur  implements MouseListener, MouseMotionListener{
	
	VueManager manager;
	Jeu jeu;
	int tour = 1;
	
	public Selecteur(VueManager manager) {
		this.manager = manager;
		jeu = manager.getJeu();
	}
	
	@Override
	public void mouseDragged(MouseEvent arg0) {
		int x = arg0.getX();
		int y = arg0.getY();
		
		manager.survol(x, y, true);
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		int x = arg0.getX();
		int y = arg0.getY();
		
		manager.survol(x, y, false);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		int x = arg0.getX();
		int y = arg0.getY();
		
		//System.out.println("Pressed");
		
		manager.clic(x, y, true);	
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		int x = arg0.getX();
		int y = arg0.getY();
		
		manager.clic(x, y, false);
		
//------------------------------------------------------Competence1----------------------------------------------------------
		/*
		if (j.getTour() ) {
			j.getJoueur(1).setSelected(false);
			j.getJoueur(2).setSelected(false);
			if (x>=1075 && x<= 1150 && y>=550 && y<=625) {
				
				arene.setDessine3(201);
				
				
				
			}
		}else  {
			j.getJoueur(2).setSelected(false);
			j.getJoueur(1).setSelected(false);
			if (x>=1075 && x<= 1150 && y>=550 && y<=625) {
				arene.setDessine3(101);
				
				
				
			}
		}
		*/
		
	}

}
