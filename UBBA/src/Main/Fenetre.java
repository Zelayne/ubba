package Main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import Controleur.Selecteur;
import Modele.Jeu;
import Vue.VueArene;
import Vue.VueManager;



public class Fenetre {
	
	public Fenetre(/*String s, int a, int b*/) {
		JFrame f= new JFrame("Ultimate Brawl Battle Arena");
		/*f.setPreferredSize(new Dimension(1200,540));*/
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		Jeu j = new Jeu();
		VueManager affichage = new VueManager(f, j, 1920, 1080);
		
		Selecteur select = new Selecteur(affichage);
		affichage.addMouseListener(select);
		affichage.addMouseMotionListener(select);
		
		f.add(new JScrollPane(affichage));
		f.pack();
		f.setVisible(true);
		
		Ressources.getSound("music").start();

		affichage.requestFocusInWindow();
	}
	
	public static void main(String[] args) {
		new Fenetre();
	}
}
