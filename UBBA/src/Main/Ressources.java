package Main;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Ressources {

	//Pourrait être une Map<String, Ressource> avec Ressource une classe gerant mieux les erreurs (meme nom pour une image et un son, loading..)
	private static Map<String, Object> ressources = new HashMap<String, Object>();
	
	private static void load(String nom, String type) throws IOException
	{	
		String[] types = {"image", "musique"};
		String[] repertoires = {"image", "musiques"};
		
		String[][] exts = {
				{ ".png", ".jpg", ".jpeg"},
				{ ".wav"}
		};
		boolean ajoute = false;
		
		int typeI = Arrays.asList(types).indexOf(type);
		if (typeI == -1)
			throw new IllegalArgumentException("Type de ressource incorrect : " + type);
		
		for(String ext : exts[typeI])
		{
			String filename = repertoires[typeI] + "/" + nom + ext;
			try {
				Object o = null;
				switch (type)
				{
					case "image":
						o = ImageIO.read(new File(filename));
						break;
					case "musique":
						Clip clip = AudioSystem.getClip();
						clip.open(
								AudioSystem.getAudioInputStream(
										new BufferedInputStream(
												new FileInputStream(filename))));
						o = clip;
						break;
				}
				ressources.put(nom, o);
				ajoute = true;
				break;
			} catch (IOException | LineUnavailableException e){}
			catch(UnsupportedAudioFileException e) {
				e.printStackTrace();
				System.err.println("Format non supporté pour le fichier " + filename);
			}
		}
		
		if (!ajoute)
			throw new IOException("La ressource " + nom + " de type " + type + " n'existe pas ou ne peut pas être ouverte");
	}
	
	private static Object get(String nom, String type) {

		if (! ressources.containsKey(nom))
		{
			try{
				load(nom, type);
			} catch(IOException e)
			{
				System.err.println(e.getMessage());
				return null;
			}
		}
		
		return ressources.get(nom);
	}
	
	public static Image getImage(String nom)
	{
		return (Image) get(nom, "image");
	}

	public static Clip getSound(String nom) {
		return (Clip) get(nom, "musique");
	}
}
