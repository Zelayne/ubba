package Modele.Champions;

import Main.Ressources;
import Modele.Champion;
import Modele.Competences.Fruitière;
import Modele.Competences.Sabrière;

public class Teko extends Champion{
	
	public Teko() {
		super("Teko", 12, 4, 5);
		une = new Sabrière("Sabrière : Teko tranche sa cible et inflige 3 dégats d'attaque physique à sa cible",Ressources.getImage("Sabrière"));
		deux = new Fruitière("Fruitière : Teko mange un de ses fruits et se régenère 3 points de santé",Ressources.getImage("Fruitière"));
	}
}
