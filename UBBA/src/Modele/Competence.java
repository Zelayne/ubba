package Modele;

import java.awt.Image;

public abstract class Competence {
	
	protected String desc;
	protected Image imgcomp;
	
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Image getImgcomp() {
		return imgcomp;
	}

	public void setImgcomp(Image imgcomp) {
		this.imgcomp = imgcomp;
	}

	public Competence(String d, Image i) {
		desc = d;
		imgcomp = i;
	}
	
	public void competenceDo(Champion c) {}
	
}
