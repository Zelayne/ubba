package Modele;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Observable;

import javax.imageio.ImageIO;

import Modele.Champion;
import Vue.VueArene;

public class Jeu extends Observable{

	Champion[] joueurs;
	boolean tour = false;
	
	Champion[][] tabChamp;
	
	public Jeu() {
		joueurs = new Champion[2];
		
		tabChamp = new Champion[10][10];

	}
	
	public boolean getTour() {
		return tour;
	}


	public void setTour(boolean tour) {
		this.tour = tour;
		
		int joueur = tour ? 2 : 1;
		Champion j = getJoueur(joueur);
		
		j.setPa(j.getPaMax());
		j.setPm(j.getPmMax());
	}

	
	public void choisirChampion(int num, String cha) {
		joueurs[num - 1] = Caserne.donnerChampion(cha);
	}
	
	public void placerJoueur() {
		tabChamp[0][0] = getJoueur(1);
		tabChamp[9][9] = getJoueur(2);
		joueurs[0].setPosX(10);
		joueurs[0].setPosY(10);
		joueurs[1].setPosX(910);
		joueurs[1].setPosY(910);
		joueurs[0].setPosI(0);
		joueurs[0].setPosJ(0);
		joueurs[1].setPosI(9);
		joueurs[1].setPosJ(9);
		
	}
	
	public boolean finJeu() {
		for (Champion j : joueurs)
		{
			if (j != null && j.getPv() <= 0)
				return true;
		}
		return false;
	}

	public Champion getJoueur(int num) {
		return joueurs[num - 1];
	}

}
