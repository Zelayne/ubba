package Vue;

public class CreateurVue {
	
	public static Vue creer(int etape, VueManager vm)
	{
		switch(etape)
		{
			case VueManager.ETAPE_ACCUEIL:
				return new VueAccueil(vm);
			case VueManager.ETAPE_SEL:
				return new VueSelection(vm);
			case VueManager.ETAPE_JEU:
				return new VueArene(vm);
			case VueManager.ETAPE_FIN:
				return new VueFin(vm);
		}
		return null;
	}

}
