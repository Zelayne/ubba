package Vue;

import javax.swing.JPanel;

public abstract class Vue extends JPanel{
	
	public abstract void clic(int x, int y, boolean enfonce);
	public abstract void survol(int x, int y, boolean enfonce);
}
