package Vue;

import java.awt.Graphics;

import javax.swing.JPanel;

import Main.Ressources;

public class VueFin extends Vue {

	public VueFin(VueManager vm) {
	}

	@Override
	public void paintComponent(Graphics g)
	{
		g.drawImage(Ressources.getImage("beau"), 0, 0, null);
	}

	@Override
	public void clic(int x, int y, boolean enfonce) {
	}

	@Override
	public void survol(int x, int y, boolean enfonce) {
	}
}
