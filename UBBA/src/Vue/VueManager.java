package Vue;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

import Modele.Jeu;

public class VueManager extends JPanel{
	
	private int step;
	private Vue vue;
	private JFrame fenetre;
	private Jeu jeu;
	
	public static final int ETAPE_ACCUEIL = 1, ETAPE_SEL = 2, ETAPE_JEU = 3, ETAPE_FIN = 4;
	
	public VueManager(JFrame f, Jeu j, int w, int h)
	{
		fenetre = f;
		jeu = j;
		step = 0;
		
		setPreferredSize(new Dimension(w, h));
		
		next();
	}
	
	private void initStep()
	{
		switch(step)
		{
		case ETAPE_JEU:
			jeu.placerJoueur();
			break;
		}
	}
	
	private void changerVue()
	{
		vue = CreateurVue.creer(step, this);
		removeAll();
		add(vue);
		vue.setPreferredSize(getPreferredSize());
		revalidate();
		repaint();
	}

	public void next() {
		step++;
		changerVue();
		initStep();
	}

	public void prev() {
		step--;
		changerVue();
	}

	public Jeu getJeu() {
		return jeu;
	}

	public void survol(int x, int y, boolean enfonce) {
		vue.survol(x, y, enfonce);
	}

	public void clic(int x, int y, boolean enfonce) {
		vue.clic(x, y, enfonce);	
	}
}
