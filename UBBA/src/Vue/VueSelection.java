package Vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;

import Main.Ressources;

public class VueSelection extends Vue implements ActionListener{
	
	private VueManager manager;
	private JPanel panelBoutons;
	private JButton commencer, retour;

	private String[] persos;
	private int joueurSel;
	private boolean finiSelectionne;
	
	private List<BoutonPerso> boutons;

	private static final String[] PERSOS = 
		{"teko", "pierro", "mona", "chiro", "lance", "kayn", "fakin", "cadence"};
	private static final int NB_COLONNES = 4;
	
	public VueSelection(VueManager m) {
		manager = m;

		setLayout(null);
		
		panelBoutons = new JPanel();
		panelBoutons.setBackground(Color.yellow);
		panelBoutons.setBounds(830, 850, 210, 115);
		
		commencer = new JButton("COMMENCER");
		commencer.setPreferredSize(new Dimension(200,50));
		
		retour = new JButton("RETOUR");
		retour.setPreferredSize(new Dimension(200,50));
		
		panelBoutons.add(commencer);
		panelBoutons.add(retour);
		
		add(panelBoutons);
		
		commencer.addActionListener(this);
		commencer.setEnabled(false);
		
		retour.addActionListener(this);
		
		boutons = new ArrayList<BoutonPerso>();
		
		for (int i = 0; i < PERSOS.length; i++)
		{
			int xRect = 720 + (i % NB_COLONNES) * 110;
			int yRect = 290 + (i / NB_COLONNES) * 110;
			boutons.add(new BoutonPerso(xRect, yRect));
		}
		
		joueurSel = 1;
		finiSelectionne = false;	
		persos = new String[2];
	}

	public void actionPerformed(ActionEvent e) {
		JButton source = (JButton) e.getSource();
		
		if (source == commencer) 
			manager.next();
		else if (source == retour) 
			manager.prev();
	}
	
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.setColor(Color.gray);
		g.fillRect(0, 0, 1920, 1080);
		
		g.drawImage(Ressources.getImage("beau"), 0, 0, null);
		
		g.setColor(Color.WHITE);
		g.fillRect(700, 162, 480, 50);
		
		g.setColor(Color.BLACK);
		g.drawRect(700, 162, 480, 50);
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("Helvetica", Font.PLAIN, 30));
		g.drawString("SELECTIONS DES CHAMPIONS", 720, 200);
		
		
		g.setColor(Color.WHITE);
		g.fillRect(100, 100, 500, 50);
		g.fillRect(1300, 100, 500, 50);
		
		g.setColor(Color.BLUE);
		g.setFont(new Font("Arial", Font.PLAIN, 30));
		g.drawString("JOUEUR 1", 260, 140);
		
		g.setColor(Color.RED);
		g.setFont(new Font("Arial", Font.PLAIN, 30));
		g.drawString("JOUEUR 2", 1460, 140);
		
		g.drawImage(Ressources.getImage("splash_temp"), 100, 150, null);
		g.drawImage(Ressources.getImage("splash_temp"), 1300, 150, null);
		
		g.drawImage(Ressources.getImage("VS"), 800, 550, null);

		
		for (BoutonPerso b : boutons)
			b.afficher(g);
		
		
		for (int i = 0; i < joueurSel; i++)
		{
			if (persos[i] != null)
				g.drawImage(Ressources.getImage("splash_" + persos[i]), i == 0 ? 100 : 1300, 150, null);
		}
		
		for (int i = 0; i < PERSOS.length; i++)
		{
			String s = PERSOS[i];
			int x = 725 + (i % NB_COLONNES) * 110;
			int y = 295 + (i / NB_COLONNES) * 110;
			g.drawImage(Ressources.getImage("icone_" + s), x, y, null);
		}
		
	}

	public void setPerso(String name, boolean survol) {
		persos[joueurSel - 1] = name;
		if (!survol)
			manager.getJeu().choisirChampion(joueurSel, name);
	}
	
	@Override
	public void clic(int x, int y, boolean enfonce) {
		if (finiSelectionne || !enfonce)
			return;
		
		for(int i = 0; i < boutons.size(); i++)
		{
			BoutonPerso b = boutons.get(i);
			if (b.selection(x, y, joueurSel))
			{
				setPerso(PERSOS[i], false);
				if (joueurSel == 2) {
					finiSelectionne = true;
					commencer.setEnabled(true);
				}
				else
					joueurSel++;
			}
		}
		repaint();	
		
	}

	@Override
	public void survol(int x, int y, boolean enfonce) {
		if (finiSelectionne)
			return;
			
		for(int i = 0; i < boutons.size() ; i++)
		{
			BoutonPerso b = boutons.get(i);
			if (b.survol(x, y))
				setPerso(PERSOS[i], true);
		}
		repaint();	
	}
	
}
